<?php
use Page\Registration as Registration;

class ApiCest
{
    /**
     * @env api
     * @group reg
     */
    public function testRegistrationApi(ApiTester $I)
    {
        $I->wantTo("ensure Registration API includes specified data and doesn't allow registration with invalid data");

        foreach (Registration::$testCases as $testCase) {

            $jsonData = $I->setConditions($testCase);

            $jsonDataEncoded = json_encode($jsonData);

            $I->sendPOST(Registration::$regApiEndpoint, $jsonDataEncoded);

            $I->seeResponseIsJson();

            $result = $I->grabDataFromResponseByJsonPath('$.Result')[0];
            $details = $I->grabDataFromResponseByJsonPath('$.Details')[0];

            var_dump($result);
            var_dump($details);
            var_dump($testCase);

            if (($details !== $testCase) || ($result !== 0)) {

                $I->addError("Expected Details: $testCase\nActual Details: $details");
            }
        }
        $I->displayErrors();
    }


    /**
     * @env api
     * @group auth
     */
    public function testAuthorizationApi(ApiTester $I)
    {
        $I->wantTo("ensure Authorization API works and returns correct data");

        foreach (Registration::$credentialsPairs as $credentials) {

            $jsonDataEncoded = json_encode($credentials[0]);

            $I->sendPOST(Registration::$authApiEndpoint, $jsonDataEncoded);

            $I->seeResponseIsJson();

            $result = $I->grabDataFromResponseByJsonPath('$.Result')[0];
            $details = $I->grabDataFromResponseByJsonPath('$.Details')[0];

            $expectedResult = $credentials[2]['Result'];
            $expectedDetails = $credentials[1]['Details'];

            if ($result != $expectedResult) {
                $I->addError("Expected Result: $expectedResult\nActual Result: $result\n");
            }
            if ($details != $expectedDetails) {
                $I->addError("Expected Details: $expectedDetails\nActual Details: $details\n");
            }
        }
        $I->displayErrors();
    }
}