<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    /************** Error handling functions **************/

    /**
     * @var array - used to store found errors for further output
     */
    public $errors = [];

    /**
     * Add error message to @errors array
     * Used in further errors display
     *
     * @param $error
     */
    public function addError($error)
    {
        array_push($this->errors, $error);
    }

    /**
     * @throws \Exception
     */
    public function displayErrors()
    {
        if (!empty($this->errors)) {
            throw new \Exception("Errors: \n" . implode("\n", $this->errors));
        }
    }

    /************** Registration API functions **************/

    /**
     * Sets invalid values
     * @param $testCase
     * @return array
     */
    public function setConditions($testCase)
    {
        self::resetConditions();

        switch ($testCase) {

            case 'Field email missed':
                unset(\Page\Registration::$dataSet['email']);
                break;

            case 'Field email bad format':
                \Page\Registration::$dataSet['email'] = str_shuffle('/[\/:*?"<>|]/');
                break;

            case 'Field email already exists':
                \Page\Registration::$dataSet['email'] = 'a@b.com';
                break;

            case 'Field birthDate missed':
                unset(\Page\Registration::$dataSet['birthDate']);
                break;

            case 'Field birthDate bad format':
                \Page\Registration::$dataSet['birthDate'] = rand(1996,2017).'-06-25T00:00:00.000Z';
                break;

            case 'Field phone missed':
                unset(\Page\Registration::$dataSet['phone']);
                break;

            case 'Field pwd bad format':
                \Page\Registration::$dataSet['pwd'] = str_shuffle('/[\/:*?"<>|]/');
                break;

            case 'Field description missed':
                unset(\Page\Registration::$dataSet['description']);
                break;
        }
        return \Page\Registration::$dataSet;
    }

    /**
     * Sets valid values
     */
    private function resetConditions()
    {
        \Page\Registration::$dataSet['email'] = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 6).'@b.com';
        \Page\Registration::$dataSet['birthDate'] = '1988-06-25T00:00:00.000Z';
        \Page\Registration::$dataSet['phone'] =  '+'.rand(1111111111,9999999999);
        \Page\Registration::$dataSet['pwd'] = substr(base_convert((mt_rand()), 16, 36), 0, 10);
    }
}
