<?php
namespace Page;

class Registration
{
    public static $regApiEndpoint = '/v1/register';
    public static $authApiEndpoint = '/v1/authorize';

    public static $dataSet = [
        'email' => 'ayy@yyb.com',
        'phone' => '+371 6111111',
        'pwd' => '6k549fd3kd3',
        'birthDate' => '1988-06-25T00:00:00.000Z',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        'address' => [
            'country' => 'US',
            'city' => 'New York',
            'state' => 'John Doe',
            'zip' => 'LV-1011',
            'street' => 'Ropazu 10'
        ]
    ];

    public static $testCases = [
        'Field email missed', //<--obligatory field
        'Field email bad format',
        'Field email already exists',
        'Field birthDate missed', //<--obligatory field
        'Field birthDate bad format',
        'Field phone missed',
        'Field pwd bad format',
        'Field description missed' //<--obligatory field
    ];

    public static $credentialsPairs = [
        [
            ["login" => "8nbfyb@b.com", "pwd" => "1kr0nl2c"],
            ["Details" => "AAABBBCCCDDDEEE=="],
            ["Result" => 1],
        ],

        [
            ["login" => "auyfgv@b.com", "pwd" => "kjh45dss"],
            ["Details" => "Failed to authorize"],
            ["Result" => 0]
        ]
    ];
}